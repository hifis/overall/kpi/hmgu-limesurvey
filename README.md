# Service Usage

* HMGU LimeSurvey

## Plotting

* Plotting is be performed in the [Plotting project](https://gitlab.hzdr.de/hifis/overall/kpi/kpi-plots-ci).

## Data + Weighing

| Data | Weighing | Comment |
| ----- | ----- | ----- |
| total surveys | 0% | Nr of all surveys |
| active surveys | 0% | Nr of active surveys |
| users | 0% | Nr of partaking users of active surveys |
| survey admins | 0% | Nr of users creating and administrating surveys |
| survey admin groups | 0% | Nr of groups of survey admins |
| total questions of active surveys | 0% | Sum of questions in all active surveys |
| unique institutes | 0% | Nr of unique institutes |
| new surveys within the week | 0% | Nr of new surveys in last consideration period |

## Schedule

* weekly
